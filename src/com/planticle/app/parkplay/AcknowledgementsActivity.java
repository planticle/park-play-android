package com.planticle.app.parkplay;

import com.planticle.app.parkplay.model.*;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.PixelFormat;
import android.view.View;
import android.graphics.*;
import android.graphics.drawable.*;
import android.widget.*;
import android.content.*;
import android.util.Log;
import android.support.v4.app.NavUtils;
import android.text.*;
import android.text.method.*;
import android.net.Uri;

import org.apache.commons.io.IOUtils;
import com.squareup.picasso.Picasso;
import com.actionbarsherlock.app.*;
import com.actionbarsherlock.view.*;

public class AcknowledgementsActivity extends SherlockActivity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.acknowledgements);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView textView = (TextView) findViewById(R.id.text);

        try {
            String html = IOUtils.toString(getAssets().open("acknowledgements.html"), "UTF-8");
            textView.setText(Html.fromHtml(html));
            textView.setMovementMethod(LinkMovementMethod.getInstance());
        } catch(Exception e) {
            throw new RuntimeException(e);
        }

        AndroidUtil.setTypefaceForAllDescendantTextViews(this, textView, "RobotoSlab-Regular");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}
