package com.planticle.app.parkplay;

import com.planticle.app.parkplay.model.*;
import com.planticle.app.parkplay.model.Settings.SortMode;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.*;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.*;
import android.content.*;
import android.util.Log;
import android.support.v4.app.NavUtils;
import android.text.*;
import android.text.method.*;
import android.app.AlertDialog;
import android.location.Location;

import com.actionbarsherlock.app.*;
import com.actionbarsherlock.view.*;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.google.android.gms.location.*;

import java.util.*;

public class MapFragment extends SherlockMapFragment
{
    private static final int DEFAULT_ZOOM = 14;

    private GoogleMap map;
    private View rootView;
    private boolean waitingForLocation = true;
    private List<Playground> playgrounds;
    private Map<String, Playground> markerMap = new HashMap<String, Playground>();
    private Map<Playground, Marker> playgroundMap = new HashMap<Playground, Marker>();
    private Set<Integer> lastFavourites;
    private Marker sortPinMarker;

    private Playground playgroundToSelect;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        playgrounds = Collections.emptyList();

        Data.getInstanceAsync(getActivity(), new Data.Callback() {
            public void run(Data data) {
                updatePlaygrounds(data.getFilteredPlaygrounds());
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = super.onCreateView(inflater, container, savedInstanceState);
        
//        initMap();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(map == null) initMap();
        else updateMap();
    }

    public void selectPlayground(Playground playground) {
        Log.v("ParkPlay", "Select playground");
        waitingForLocation = false;
        Marker marker = playgroundMap.get(playground);
        if(marker != null) {
            marker.showInfoWindow();
            map.moveCamera(CameraUpdateFactory.newLatLng(playground.getLatLng()));
        } else {
            playgroundToSelect = playground;
        }
    }

    public void onInfoWindowTap(Playground playground) {
        //Log.v("ParkPlay", "Playground marker tapped: " + playground.name);

        Intent intent = new Intent(getActivity(), PlaygroundDetailsActivity.class);
        intent.putExtra("playgroundId", playground.id);
        startActivity(intent);
    }

    private void initMap() {
        //Log.v("ParkPlay", "initMap");
        if(map == null) {
            //Log.v("ParkPlay", ">>initMap");
            map = getMap();
            if(map != null) {
                //Log.v("ParkPlay", ">>>>initMap");

                final BitmapDescriptor SORT_PIN_MARKER_ICON = BitmapDescriptorFactory.fromResource(R.drawable.pin_drop);

                map.setMyLocationEnabled(true);

                LatLng adelaide = new LatLng(-34.9290, 138.6010);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(getLastLatLng(adelaide), DEFAULT_ZOOM));

                map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    public void onInfoWindowClick(Marker marker) {
                        if(marker.equals(sortPinMarker)) {
                            //Log.v("ParkPlay", "Tapped sort pin info window");
                            marker.hideInfoWindow();
                            Settings settings = Data.getInstance().settings;
                            settings.sortMode = SortMode.DISTANCE_FROM_PIN;
                            ((MainActivity)getActivity()).sortModeUpdated(true);
                        } else {
                            onInfoWindowTap(markerMap.get(marker.getId()));
                        }
                    }
                });

                map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    public void onMapLongClick(LatLng point) {
                        if(sortPinMarker != null) {
                            sortPinMarker.remove();
                        }
                        sortPinMarker = map.addMarker(new MarkerOptions()
                            .position(point)
                            .draggable(true)
                            .title("Search Pin")
                            .snippet("View nearby playgrounds")
                            .icon(SORT_PIN_MARKER_ICON)
                        );

                        Settings settings = Data.getInstance().settings;
                        settings.sortPinLat = point.latitude;
                        settings.sortPinLon = point.longitude;
                        ((MainActivity)getActivity()).sortModeUpdated(false);

                        sortPinMarker.showInfoWindow();
                        map.animateCamera(CameraUpdateFactory.newLatLng(point));
                    }
                });

                map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    public void onMarkerDrag(Marker marker) {}
                    public void onMarkerDragStart(Marker marker) {}
                    public void onMarkerDragEnd(Marker marker) {
                        Settings settings = Data.getInstance().settings;
                        settings.sortPinLat = marker.getPosition().latitude;
                        settings.sortPinLon = marker.getPosition().longitude;
                        ((MainActivity)getActivity()).sortModeUpdated(false);
                    }
                });

                map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    private View contentView;

                    @Override
                    public View getInfoContents(Marker marker) {
                        if(contentView == null) {
                            contentView = getActivity().getLayoutInflater().inflate(R.layout.info_window, null);
                        }
                        TextView tvTitle = ((TextView)contentView.findViewById(R.id.title));
                        tvTitle.setText(marker.getTitle());
                        TextView tvSnippet = ((TextView)contentView.findViewById(R.id.snippet));
                        tvSnippet.setText(marker.getSnippet());

                        return contentView;
                    }

                    @Override
                    public View getInfoWindow(Marker marker) {
                        return null;
                    }
                });

                Data.getInstanceAsync(getActivity(), new Data.Callback() {
                    public void run(Data data) {
                        createMarkers(data.playgrounds);
                        updateMap();
                    }
                });
            }
        }
    }

    private LocationClient getLocationClient() {
        return ((MainActivity)getActivity()).locationClient;
    }

    private LatLng getLastLatLng(LatLng defaultLatLng) {
        LatLng latlng = getLastLatLng();
        return latlng == null ? defaultLatLng : latlng;
    }

    private LatLng getLastLatLng() {
        LocationClient client = getLocationClient();
        if(client == null || !client.isConnected()) return null;
        Location location = client.getLastLocation();
        if(location == null) return null;
        LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
        return latlng;
    }

    public void updatePlaygrounds(List<Playground> playgrounds) {
        this.playgrounds = playgrounds;
        updateMap();
    }

    public void locationClientConnected() {
        waitingForLocation = false;

        // Center on users location
        // Log.v("ParkPlay", "location client connected: " + getLocationClient().isConnected());
        LatLng userLatLng = getLastLatLng();
        if(userLatLng != null && map != null) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, DEFAULT_ZOOM));
        }

        ensurePlaygroundVisible();
    }

    private static <T> Set<T> xor(Set<T> a, Set<T> b) {
        Set<T> c = new HashSet<T>(a);
        c.removeAll(b);

        Set<T> d = new HashSet<T>(b);
        d.removeAll(a);

        c.addAll(d);

        return c;
    }

    private void updateMap() {
        // Log.v("ParkPlay", "updateMap");
        if(map != null && playgroundMap.size() > 0) {
            // Log.v("ParkPlay", ">>updateMap");
            final BitmapDescriptor DEFAULT_MARKER_ICON = BitmapDescriptorFactory.fromResource(R.drawable.pin);
            final BitmapDescriptor FAVOURITE_MARKER_ICON = BitmapDescriptorFactory.fromResource(R.drawable.pin_favourite);

            Set<Playground> visiblePlaygrounds = new HashSet<Playground>(playgrounds);
            Set<Playground> hiddenPlaygrounds = new HashSet<Playground>(playgroundMap.keySet());
            hiddenPlaygrounds.removeAll(visiblePlaygrounds);

            // Work out which playgrounds have changed their favourite status
            Set<Integer> newFavourites = new FavouritesManager(getActivity()).getFavourites();
            Set<Integer> changedFavourites = xor(lastFavourites, newFavourites);
            lastFavourites = newFavourites;

            for(Playground playground : visiblePlaygrounds) {
                final Marker marker = playgroundMap.get(playground);
                if(changedFavourites.contains(playground.id)) {
                    // To get the icon to change while keeping the info window shown, we have to do this
                    boolean infoWindowShown = marker.isInfoWindowShown();
                    marker.hideInfoWindow();
                    marker.setIcon(playground.favourite ? FAVOURITE_MARKER_ICON : DEFAULT_MARKER_ICON);
                    if(infoWindowShown) {
                        rootView.post(new Runnable() {
                            public void run() {
                                marker.showInfoWindow();
                            }
                        });
                    }
                }
                marker.setVisible(true);
            }

            for(Playground playground : hiddenPlaygrounds) {
                playgroundMap.get(playground).setVisible(false);
            }

            if(playgroundToSelect != null) {
                Playground playground = playgroundToSelect;
                playgroundToSelect = null;
                selectPlayground(playground);
            } else {
                ensurePlaygroundVisible();
            }
        }
    }

    private void createMarkers(List<Playground> playgrounds) {
        // Log.v("ParkPlay", "createMarkers");
        if(!markerMap.isEmpty()) {
            // Log.v("ParkPlay", "createMarkers called multiple times!!");
            return;
        }

        final BitmapDescriptor DEFAULT_MARKER_ICON = BitmapDescriptorFactory.fromResource(R.drawable.pin);
        final BitmapDescriptor FAVOURITE_MARKER_ICON = BitmapDescriptorFactory.fromResource(R.drawable.pin_favourite);

        MarkerOptions markerOptions = new MarkerOptions().visible(false);

        for(Playground playground : playgrounds) {
            Marker marker = map.addMarker(markerOptions
                .position(playground.getLatLng())
                .title(playground.name)
                .snippet(playground.address)
                .icon(playground.favourite ? FAVOURITE_MARKER_ICON : DEFAULT_MARKER_ICON)
            );
            markerMap.put(marker.getId(), playground);
            playgroundMap.put(playground, marker);
        }

        lastFavourites = new FavouritesManager(getActivity()).getFavourites();
    }

    private void ensurePlaygroundVisible() {
        if(waitingForLocation) return;
        if(playgrounds.isEmpty()) return;
        if(map == null) return;

        rootView.post(new Runnable() {

            public void run() {
                // Ensure nearest playground is on map
                LatLng currentTarget = map.getCameraPosition().target;
                Playground nearestPlayground = Data.getNearestPlayground(playgrounds, locationFromLatLng(currentTarget));
                Point p = map.getProjection().toScreenLocation(nearestPlayground.getLatLng());
                Rect rect = new Rect();
                rootView.getDrawingRect(rect);

                if(rect.width() == 0 || rect.height() == 0) {
                    Log.w("ParkPlay", "Unable to ensure playground visible as 0 view dimensions reported.");
                    return;
                }

                if(!rect.contains(p.x, p.y)) {
                    LatLngBounds bounds = new LatLngBounds.Builder()
                        .include(currentTarget)
                        .include(nearestPlayground.getLatLng())
                        .build();

                    // Log.v("ParkPlay", this.getResources().getDisplayMetrics().widthPixels + ", " + this.getResources().getDisplayMetrics().heightPixels);
                    // Log.v("ParkPlay", rect.width() + ", " + rect.height());
                    map.animateCamera(CameraUpdateFactory.newLatLngBounds(
                        bounds,
                        rect.width(),
                        rect.height(),
                        Math.min(rect.width(), rect.height()) * 1/4));
                }
            }
        });
    }

    private Location locationFromLatLng(LatLng latLng) {
        Location location = new Location("");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }
}
