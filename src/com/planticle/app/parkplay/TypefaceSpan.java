package com.planticle.app.parkplay;

import android.os.Bundle;
import android.view.ViewGroup;
import android.view.View;
import android.view.WindowManager;
import android.graphics.*;
import android.widget.*;
import android.content.*;
import android.text.*;
//import android.app.*;
import android.util.*;
import android.app.Activity;
import java.util.*;
import android.text.style.*;

/*
 * Copyright 2013 Simple Finance Corporation. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
/**
 * Style a {@link Spannable} with a custom {@link Typeface}.
 * 
 * @author Tristan Waddington
 * @author James Watmuff - use ttf instead of otf, don't use lru cache, add factory
 */
public class TypefaceSpan extends MetricAffectingSpan {
      /** An <code>LruCache</code> for previously loaded typefaces. */
      /*
    private static LruCache<String, Typeface> sTypefaceCache =
            new LruCache<String, Typeface>(12);
            */

    private static Map<String, Typeface> sTypefaceCache = new HashMap<String, Typeface>();
 
    private Typeface mTypeface;

    public static class Factory {
        Context context;
        String typefaceName;

        public Factory(Context context, String typefaceName) {
            this.context = context;
            this.typefaceName = typefaceName;
        }

        public SpannableString getString(String text) {
            SpannableString s = new SpannableString(text);
            s.setSpan(new TypefaceSpan(context, typefaceName), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return s;
        }
    }
 
    /**
     * Load the {@link Typeface} and apply to a {@link Spannable}.
     */
    public TypefaceSpan(Context context, String typefaceName) {
        mTypeface = sTypefaceCache.get(typefaceName);
 
        if (mTypeface == null) {
            mTypeface = Typeface.createFromAsset(context.getApplicationContext()
                    .getAssets(), String.format("fonts/%s.ttf", typefaceName));
 
            if(sTypefaceCache.size() > 12) sTypefaceCache.clear(); // Crude replacement for LruCache

            // Cache the loaded Typeface
            sTypefaceCache.put(typefaceName, mTypeface);
        }
    }
 
    @Override
    public void updateMeasureState(TextPaint p) {
        p.setTypeface(mTypeface);
        
        // Note: This flag is required for proper typeface rendering
        p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }
 
    @Override
    public void updateDrawState(TextPaint tp) {
        tp.setTypeface(mTypeface);
        
        // Note: This flag is required for proper typeface rendering
        tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }
}
