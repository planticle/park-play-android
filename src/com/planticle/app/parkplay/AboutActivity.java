package com.planticle.app.parkplay;

import com.planticle.app.parkplay.model.*;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.PixelFormat;
import android.view.View;
import android.graphics.*;
import android.graphics.drawable.*;
import android.widget.*;
import android.content.*;
import android.util.Log;
import android.support.v4.app.NavUtils;
import android.text.*;
import android.text.method.*;
import android.net.Uri;

import com.squareup.picasso.Picasso;
import com.actionbarsherlock.app.*;
import com.actionbarsherlock.view.*;

public class AboutActivity extends SherlockActivity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.about);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        AndroidUtil.setTypefaceForAllDescendantTextViews(this, findViewById(R.id.main), "RobotoSlab-Regular");

        final TextView textView = (TextView) findViewById(R.id.count);
        Data.getInstanceAsync(this, new Data.Callback() {
            public void run(Data data) {
                textView.setText("Playgrounds Listed: " + data.playgrounds.size());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }


    public void onAboutUsButtonClick(View v) {
        Intent intent = new Intent(this, AboutUsActivity.class);
        startActivity(intent);
    }

    public void onAboutAppButtonClick(View v) {
        Intent intent = new Intent(this, AboutAppActivity.class);
        startActivity(intent);
    }

    public void onAcknowledgementsButtonClick(View v) {
        Intent intent = new Intent(this, AcknowledgementsActivity.class);
        startActivity(intent);
    }

    public void onReportButtonClick(View v) {
        sendEmail("Incorrect Information in Park Play App");
    }

    public void onSuggestButtonClick(View v) {
        sendEmail("Playground Suggestion for Park Play App");
    }

    public void onLegalButtonClick(View v) {
        Intent intent = new Intent(this, LegalActivity.class);
        startActivity(intent);
    }

    private void sendEmail(String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
            "mailto","goplay@planticle.com.au", null));
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        startActivity(Intent.createChooser(intent, "Send Email"));
    }
}
