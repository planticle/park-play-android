package com.planticle.app.parkplay;

import android.os.Bundle;
import android.view.ViewGroup;
import android.view.View;
import android.view.WindowManager;
import android.graphics.*;
import android.widget.*;
import android.content.*;
import android.content.res.Resources;
import android.text.*;
//import android.app.*;
import android.util.*;
import android.support.v4.app.NavUtils;
import android.app.Activity;
import java.util.*;

public class AndroidUtil {
    public static Typeface getTypeface(Context c, String fontName) {
        return Typeface.createFromAsset(c.getAssets(), "fonts/" + fontName + ".ttf");
    }

    public static void setTypefaceForAllDescendantTextViews(Context c, View v, String fontName) {
        setTypefaceForAllDescendantTextViews(c, v, getTypeface(c, fontName));
    }

    public static void setTypefaceForAllDescendantTextViews(Context c, final View v, final Typeface typeface) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    setTypefaceForAllDescendantTextViews(c, child, typeface);
                }
            } else if (v instanceof TextView) {
                ((TextView)v).setTypeface(typeface);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // ignore
        }
    }

    public static float dpToPx(Context c, float dp) {
        Resources r = c.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return px;
    }
}