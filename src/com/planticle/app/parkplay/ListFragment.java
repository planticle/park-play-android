package com.planticle.app.parkplay;

import com.planticle.app.parkplay.model.*;
import com.planticle.app.parkplay.model.Settings.SortMode;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.*;
import android.graphics.drawable.*;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.*;
import android.content.*;
import android.util.Log;
import android.support.v4.app.NavUtils;
import android.text.*;
import android.text.method.*;
import android.net.Uri;
import android.app.AlertDialog;
import android.location.Location;
import com.google.android.gms.location.*;
import com.google.android.gms.common.*;

import com.actionbarsherlock.app.*;
import com.actionbarsherlock.view.*;

import java.util.*;

public class ListFragment extends SherlockListFragment
{
    private List<Playground> playgrounds = new ArrayList<Playground>();
    private ArrayAdapter<Playground> adapter;
    private Location location = null;
    private ListView listView;
    private static final Location ADELAIDE;
    private SortMode previousSortMode = null;

    private boolean fastScrollEnabled;

    static {
        ADELAIDE = new Location("ParkPlay");
        ADELAIDE.setLatitude(-34.9290);
        ADELAIDE.setLongitude(138.6010);
    }

    static class PlaygroundListAdapter extends ArrayAdapter<Playground> implements SectionIndexer {
        private static String[] sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("(?!^)");

        public PlaygroundListAdapter(Context context, int resource, int textViewResourceId, List<Playground> objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Playground playground = getItem(position);
            View view = super.getView(position, convertView, parent);

            view.setTag(R.id.playground, playground);
//                view.removeOnLayoutChangeListener(layoutListener);
//                view.addOnLayoutChangeListener(layoutListener);
            
            populateText(view, R.id.name, playground.name);
            populateText(view, R.id.address, playground.address);
            populateText(view, R.id.distance, playground.getDistanceString());

            view.findViewById(R.id.favourite).setVisibility(playground.favourite ? View.VISIBLE : View.INVISIBLE);
            
            LinearLayout iconHolder = (LinearLayout)view.findViewById(R.id.icons);
            iconHolder.removeAllViews();
            for(Facility facility : playground.facilities.keySet()) {
                int iconId = MainActivity.getId(facility.getIconName(), R.drawable.class);
                ImageView iconView = new ImageView(getContext());
                iconView.setImageResource(iconId);
                iconView.setPadding(5,5,5,5);
                iconHolder.addView(iconView);
            }
            for(Equipment equipment : playground.equipment.keySet()) {
                int iconId = MainActivity.getId(equipment.getIconName(), R.drawable.class);
                ImageView iconView = new ImageView(getContext());
                iconView.setImageResource(iconId);
                iconView.setPadding(5,5,5,5);
                iconHolder.addView(iconView);
            }

            return view;
        }

        private char getIndexChar(int position) {
            Playground playground = getItem(position);
            char c = playground.name.charAt(0);
            return (Character.isLetter(c)) ? Character.toUpperCase(c) : 'A';
        }

        @Override
        public int getPositionForSection(int section) {
            // Log.v("ParkPlay", "getPosForSection: " + section);
            for(int i = 0; i < getCount(); i++) {
                if(getIndexChar(i) >= sections[section].charAt(0)) return i;
            }
            return getCount() - 1;
        }

        @Override
        public int getSectionForPosition(int position) {
            Playground playground = getItem(position);
            return Arrays.asList(sections).indexOf("" + getIndexChar(position));
        }

        @Override
        public Object[] getSections() {
            return sections;
        }
    }

    private static void populateText(View view, int id, String text) {
        ((TextView)view.findViewById(id)).setText(text);
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

/*
        final View.OnLayoutChangeListener layoutListener = new View.OnLayoutChangeListener() {
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                Log.v("ParkPlay", "View width: " + (right - left));
            }
        };
*/

        LocationClient locationClient = ((MainActivity)getActivity()).locationClient;
        if(locationClient != null && locationClient.isConnected()) {
            locationClientConnected();
        }

        adapter = new PlaygroundListAdapter(getActivity(), R.layout.playground_list_item, R.id.name, playgrounds);

        setListAdapter(adapter);

        Data.getInstanceAsync(getActivity(), new Data.Callback() {
            public void run(Data data) {
                updatePlaygrounds(data.getFilteredPlaygrounds());
            }
        });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = getListView();
        listView.setDivider(new ColorDrawable(0x22000000));
        listView.setDividerHeight((int)Math.ceil(AndroidUtil.dpToPx(getActivity(), 2)));
        listView.setBackgroundColor(0xffdfd5a8);
        listView.setFastScrollEnabled(fastScrollEnabled);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Playground playground = adapter.getItem(position);

        // Log.v("ParkPlay", "Click happened: " + position + ", " + playground);

        Intent intent = new Intent(getActivity(), PlaygroundDetailsActivity.class);
        intent.putExtra("playgroundId", playground.id);
        startActivity(intent);
    }

    private void updateList() {
        if(adapter != null) {
            adapter.clear();
            for(Playground p : playgrounds) adapter.add(p);
            //adapter.addAll(playgrounds);
            adapter.notifyDataSetChanged();
        }
    }

    private void sortByLocation(Location location) {
        // Log.v("ParkPlay", "ListFragment:sortByLocation:" + location);
        if(location == null) return;
        Data.populateDistance(playgrounds, location);
        Collections.sort(playgrounds, new Comparator<Playground>() {
            public int compare(Playground p1, Playground p2) {
                return (p1.distance > p2.distance) ? 1 : (p1.distance < p2.distance) ? -1 : 0;
            }
        });
    }

    private void sortAlphabetically() {
        Collections.sort(playgrounds, new Comparator<Playground>() {
            public int compare(Playground p1, Playground p2) {
                return p1.name.compareToIgnoreCase(p2.name);
            }
        });
    }

    public void updatePlaygrounds(List<Playground> playgrounds) {
        this.playgrounds = playgrounds;
        sortPlaygrounds();
    }

    private SortMode getSortMode() {
        SortMode mode = SortMode.DISTANCE_FROM_ME;
        if(Data.getInstance() != null) {
            mode = Data.getInstance().settings.sortMode;
        }
        return mode;
    }

    public void sortPlaygrounds() {
        SortMode mode = getSortMode();

        if(previousSortMode != mode) {
            previousSortMode = mode;
            if(listView != null) {
                listView.post(new Runnable() {
                    public void run() {
                        listView.setSelectionAfterHeaderView();
                    }
                });
            }
        }

        setFastScrollEnabled(false);

        switch(mode) {
        case DISTANCE_FROM_ME:
            if(location == null)
                sortByLocation(ADELAIDE);
            else
                sortByLocation(location);
            break;
        case DISTANCE_FROM_PIN:
            Location l = new Location("");
            l.setLatitude(Data.getInstance().settings.sortPinLat);
            l.setLongitude(Data.getInstance().settings.sortPinLon);
            sortByLocation(l);
            break;
        case ALPHABETICAL:
            sortAlphabetically();
            break;
        }

        updateList();

        if(mode == SortMode.ALPHABETICAL) {
            setFastScrollEnabled(true);
        }
    }

    private void setFastScrollEnabled(boolean fastScrollEnabled) {
        this.fastScrollEnabled = fastScrollEnabled;
        if(listView != null) {
            listView.setFastScrollEnabled(fastScrollEnabled);
        }
    }

    public void locationClientConnected() {
        // Log.v("ParkPlay", "ListFragment:locationClientConnected");

        LocationClient locationClient = ((MainActivity)getActivity()).locationClient;
        LocationRequest locationRequest = LocationRequest
            .create()
            .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
            .setInterval(30000);

        setLocation(locationClient.getLastLocation());

        locationClient.requestLocationUpdates(locationRequest, new LocationListener() {
            public void onLocationChanged(Location location) {
                setLocation(location);
            }
        });
    }

    // Calls to this method may come from a non-UI thread
    private void setLocation(final Location newLocation) {
        Activity activity = getActivity();
        if(activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run(){
                    // Log.v("ParkPlay", "ListFragment:setLocation:" + newLocation);
                    if(newLocation == null || location == newLocation) return;
                    // Only update if we have moved over 10 meters
                    if(location != null && newLocation.distanceTo(location) < 10) return;
                    location = newLocation;

                    if(getSortMode() == SortMode.DISTANCE_FROM_ME) sortPlaygrounds();
                }
            });
        }
    }
}
