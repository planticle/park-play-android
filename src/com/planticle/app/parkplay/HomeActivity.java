package com.planticle.app.parkplay;

import android.app.*;
import android.content.*;
import android.os.Bundle;
import android.view.View;
import android.graphics.*;
import android.view.*;
import com.actionbarsherlock.app.*;
import android.util.Log;
import android.os.Handler;
import com.google.analytics.tracking.android.EasyTracker;

import com.planticle.app.parkplay.model.*;

public class HomeActivity extends SherlockActivity
{
    private boolean splashScreenShown = false;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        getWindow().setFormat(PixelFormat.RGBA_8888);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DITHER);

        super.onCreate(savedInstanceState);

        if(savedInstanceState != null) {
            splashScreenShown = savedInstanceState.getBoolean("splashScreenShown", false);
        }
        showSplashScreen();

        setContentView(R.layout.home);
        //Data.load(this);

        AndroidUtil.setTypefaceForAllDescendantTextViews(this, findViewById(R.id.main), "RobotoSlab-Regular");
    }

    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("splashScreenShown", splashScreenShown);
        super.onSaveInstanceState(outState);
    }

    public void showSplashScreen() {
        if(splashScreenShown == true) {
            return;
        }
        splashScreenShown = true;
        final Dialog splashDialog = new Dialog(this, R.style.SplashScreen);
        splashDialog.setContentView(R.layout.splash);
        splashDialog.setCancelable(false);
        splashDialog.show();

        // Set Runnable to remove splash screen after delay
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                splashDialog.dismiss();

                RatingDialog ratingDialog = new RatingDialog(HomeActivity.this);
                ratingDialog.showRatingDialogIfNeeded();
            }
        }, 3000);
    }

    public void onMapButtonClick(View v) {
        Intent myIntent = new Intent(this, MainActivity.class);
        myIntent.putExtra("mode", "map");
        startActivity(myIntent);
    }

    public void onListButtonClick(View v) {
        Intent myIntent = new Intent(this, MainActivity.class);
        myIntent.putExtra("mode", "list");
        startActivity(myIntent);
    }

    public void onMyFavouritesButtonClick(View v) {
        Data.getInstanceAsync(this, new Data.Callback() {
            public void run(Data data) {
                Intent myIntent = new Intent(HomeActivity.this, MainActivity.class);
                myIntent.putExtra("mode", "my_favourites");
                startActivity(myIntent);
            }
        });
    }

    public void onOurFavouritesButtonClick(View v) {
        Data.getInstanceAsync(this, new Data.Callback() {
            public void run(Data data) {
                Intent myIntent = new Intent(HomeActivity.this, MainActivity.class);
                myIntent.putExtra("mode", "our_favourites");
                startActivity(myIntent);
            }
        });
    }
}
