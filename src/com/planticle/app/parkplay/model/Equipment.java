package com.planticle.app.parkplay.model;

import java.util.*;

public class Equipment {
  public String description;
  public String name;

  public Equipment(String name) {
    this.description = name;
    this.name = name;
  }

  public String getIconName() {
    return "ic_" + name.toLowerCase().replaceAll("[^a-z]", "_");
  }
}
