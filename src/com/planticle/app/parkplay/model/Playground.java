package com.planticle.app.parkplay.model;

import java.util.*;
import android.location.Location;
import com.google.android.gms.maps.model.LatLng;

public class Playground {
  public Map<Facility, Integer> facilities;
  public Map<Equipment, Integer> equipment;
  public List<Photo> photos;

  public String street;
  public String suburb;
  public String address;
  public String description;
  public boolean favourite;
  public boolean ourFavourite;
  public double lat;
  public double lon;
  public String name;
  public int id;

  public double distance = -1;

  public Location getLocation() {
    Location l = new Location("");
    l.setLatitude(lat);
    l.setLongitude(lon);
    return l;
  }

  public LatLng getLatLng() {
    return new LatLng(lat, lon);
  }

  public String getDistanceString() {
    String str = "";

    if(distance < 0) {
      return "";
    } else if(distance < 1000) {
      return String.format("%dm", (int)distance / 10 * 10);
    } else if(distance < 10000) {
        return String.format("%10.2fkm", distance / 1000);
    } else if(distance < 100000) {
        return String.format("%10.1fkm", distance / 1000);
    } else {
        return String.format("%dkm", (int)(distance / 1000));
    }
  }
}
