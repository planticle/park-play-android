package com.planticle.app.parkplay.model;

import android.content.Context;
import android.util.*;
import android.location.Location;
import android.os.AsyncTask;

import java.util.*;
import java.io.*;
import org.apache.commons.io.IOUtils;

public class DataLoadAsyncTask extends AsyncTask <Void, Integer, Data> {
  private Context context;

  public DataLoadAsyncTask(Context context) {
    super();
    this.context = context;
  }

  protected Data doInBackground(Void... none) {
    Log.v("ParkPlay", new Date().getTime() + "");
    try {
      Data data = new Data();

      DataInputStream in = new DataInputStream(context.getAssets().open("data.bin"));

      byte[] magic = new byte[2];
      in.read(magic);

      if(!"PA".equals(new String(magic))) throw new RuntimeException("Corrupt data file - invalid header");
      int version = in.readInt();
      if(version != 1) throw new RuntimeException("Invalid data file version: expected 1, found " + version);

      readFacilities(in, data);
      readEquipment(in, data);
      readPlaygrounds(in, data);

      in.close();
      Log.v("ParkPlay", new Date().getTime() + "");

      publishProgress(100);
      return data;
    } catch(Exception e) {
      Log.e("ParkPlay", "We are in trouble", e);
      return null;
    }
  }

  //protected void onProgressUpdate(Integer... progress) {
  //  setProgressPercent(progress[0]);
  //}

  private static void readFacilities(DataInputStream in, Data data) throws IOException {
    data.facilities = new ArrayList<Facility>();

    int facilityCount = in.readInt();
    List<String> facilities = new ArrayList<String>();
    for(int i = 0; i < facilityCount; i++) {
      data.facilities.add(new Facility(in.readUTF()));
    }
  }

  private static void readEquipment(DataInputStream in, Data data) throws IOException {
    data.equipment = new ArrayList<Equipment>();

    int equipmentCount = in.readInt();
    List<String> equipment = new ArrayList<String>();
    for(int i = 0; i < equipmentCount; i++) {
      data.equipment.add(new Equipment(in.readUTF()));
    }
  }

  private void readPlaygrounds(DataInputStream in, Data data) throws IOException {
    Set<Integer> favouriteIds = new FavouritesManager(context).getFavourites();

    data.playgrounds = new ArrayList<Playground>();

    int playgroundCount = in.readInt();
    for(int i = 0; i < playgroundCount; i++) {
      Playground playground = new Playground();
      playground.facilities = new HashMap<Facility, Integer>();
      playground.equipment = new HashMap<Equipment, Integer>();
      playground.photos = new ArrayList<Photo>();

      playground.name = in.readUTF(); // Name
//Log.v("ParkPlay", playground.name);

      String access = in.readUTF(); // Access Road
      String suburb = in.readUTF(); // Suburb
      playground.id = in.readInt(); // ID
      playground.ourFavourite = in.readBoolean(); // Our Favourite
      String coords = in.readUTF(); // Coordinates
      try {
        String[] coordinates = coords.split(",");
        playground.lat = Double.valueOf(coordinates[0]);
        playground.lon = Double.valueOf(coordinates[1]);
      } catch(Exception e) {
        playground.lat = playground.lon = 0;
      }

      for(Facility f : data.facilities) {
        int count = in.readByte();
        if(count != 0) playground.facilities.put(f, count);
      }
      for(Equipment e : data.equipment) {
        int count = in.readByte();
        if(count != 0) playground.equipment.put(e, count);
      }
      readPhotos(in, playground); // Photos

      playground.address = String.format("%s, %s SA", access, suburb);
      playground.street = access;
      playground.suburb = suburb + " SA";

      playground.favourite = favouriteIds.contains(playground.id);

      data.playgrounds.add(playground);

      publishProgress(100 * data.playgrounds.size() / playgroundCount);
    }
  }

  private static void readPhotos(DataInputStream in, Playground playground) throws IOException {
    int photoCount = in.readInt();
    for(int i = 0 ; i < photoCount; i++) {
      Photo photo = new Photo();
      photo.playground = playground;

      photo.width = in.readInt();
      photo.height = in.readInt();
      photo.name = in.readUTF();

      playground.photos.add(photo);
    }
  }
}
