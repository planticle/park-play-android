package com.planticle.app.parkplay.model;

import java.util.*;
import android.content.*;
import android.util.*;
import android.text.*;

public class FavouritesManager {
    private Context context;
    private static final String KEY = "favourites";
    private static final String NONE = "none";

    public FavouritesManager(Context context) {
        this.context = context;
    }

    public Set<Integer> getFavourites() {
        SharedPreferences prefs = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
        Set<Integer> ids = new HashSet<Integer>();

        String str;
        try {
            str = prefs.getString(KEY, NONE);
        } catch(ClassCastException e) {
            prefs.edit().remove(KEY).commit();
            str = NONE;
        }
        if(str.equals(NONE)) return ids;
        for(String id : str.split("\\|")) {
            try {
                ids.add(Integer.valueOf(id));
            } catch(NumberFormatException e) {
                Log.e("ParkPlay", "Invalid number '" + id + "' in favourites list", e);
            }
        }
        return ids;
    }

    private void setFavourites(Set<Integer> ids) {
        String str = "";
        for(int id : ids) {
            if(!TextUtils.isEmpty(str)) str += "|";
            str += id;
        }
        if(TextUtils.isEmpty(str)) str = NONE;
        SharedPreferences prefs = context.getSharedPreferences(KEY, 0);
        prefs.edit().putString(KEY, str).commit();
    }

    public void addFavourite(int id) {
        Set<Integer> ids = getFavourites();
        ids.add(id);
        setFavourites(ids);
    }

    public void removeFavourite(int id) {
        Set<Integer> ids = getFavourites();
        ids.remove(id);
        setFavourites(ids);
    }
}
