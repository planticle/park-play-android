package com.planticle.app.parkplay.model;

import android.content.Context;
import android.util.*;
import android.app.ProgressDialog;
import android.location.Location;

import java.util.*;
import java.io.*;
import org.apache.commons.io.IOUtils;
import org.json.*;

public class Data {
  public static abstract class Callback {
    public abstract void run(Data data);
  }

  public static final Object FAVOURITES_ITEM = new Object();
  public static final Object OUR_FAVOURITES_ITEM = new Object();

  private static Data sharedInstance;
  private static DataLoadAsyncTask loadTask;
  private static List<Callback> loadListeners = new ArrayList<Callback>();

  public List<Facility> facilities;
  public List<Equipment> equipment;
  public List<Playground> playgrounds;
  public Settings settings = new Settings();

  /** Data Load Management **/

  public static synchronized void getInstanceAsync(final Context ctx, Callback cb) {
    if(sharedInstance != null) {

      cb.run(sharedInstance);

    } else {

      loadListeners.add(cb);

      if(loadTask == null) {
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setTitle("Loading Playgrounds");
        progressDialog.setMessage("Reading Database");
        progressDialog.setCancelable(false);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressNumberFormat(null);
        progressDialog.show();

        loadTask = new DataLoadAsyncTask(ctx) {
          protected void onProgressUpdate(Integer... progress) {
            progressDialog.setProgress(progress[0]);
          }

          protected void onPostExecute(Data data) {
            progressDialog.dismiss();
            afterLoad(data);
          }
        };
        loadTask.execute();
      }

    }
  }

  private static synchronized void afterLoad(Data data) {
    sharedInstance = data;
    for(Callback cb : loadListeners) {
      try {
        cb.run(sharedInstance);
      } catch(Exception e) {
        Log.e("ParkPlay", "Exception notifying data load listener", e);
      }
    }
    loadListeners.clear();
    loadTask = null;
  }


  // Returns null if data not yet loaded
  public static synchronized Data getInstance() {
    return sharedInstance;
  }

  /** End Data Load Management **/

  public Playground getPlayground(int id) {
    for(Playground playground : playgrounds) {
      if(playground.id == id) return playground;
    }
    return null;
  }

  public Set<Object> getSelectedItems() {
    final Set<Object> items = new HashSet<Object>();

    items.addAll(settings.selectedFacilities);
    items.addAll(settings.selectedEquipment);
    if(settings.favouriteSelected) items.add(FAVOURITES_ITEM);
    if(settings.ourFavouriteSelected) items.add(OUR_FAVOURITES_ITEM);

    return items;
  }

  public Set<Object> getUnselectableItems(Set<Object> selectedItems) {
    Set<Object> unselectable = new HashSet<Object>();

    //if(selectedItems.isEmpty()) return unselectable;

    unselectable.addAll(facilities);
    unselectable.addAll(equipment);
    unselectable.add(FAVOURITES_ITEM);
    unselectable.add(OUR_FAVOURITES_ITEM);

    for(Playground playground : getFilteredPlaygrounds(selectedItems)) {
      if(unselectable.isEmpty()) return unselectable;
      unselectable.removeAll(playground.facilities.keySet());
      unselectable.removeAll(playground.equipment.keySet());
      if(playground.favourite) unselectable.remove(FAVOURITES_ITEM);
      if(playground.ourFavourite) unselectable.remove(OUR_FAVOURITES_ITEM);
    }

    // Log.v("ParkPlay", "unselectable items: " + unselectable.size());

    return unselectable;
  }

  public Set<Object> getSelectableItems(Set<Object> selectedItems) {
    Set<Object> selectable = new HashSet<Object>();

    for(Playground playground : getFilteredPlaygrounds(selectedItems)) {
      selectable.addAll(playground.facilities.keySet());
      selectable.addAll(playground.equipment.keySet());
      if(playground.favourite) selectable.add(FAVOURITES_ITEM);
      if(playground.ourFavourite) selectable.add(OUR_FAVOURITES_ITEM);
    }

    return selectable;
  }

  public List<Playground> getFilteredPlaygrounds(Set<Facility> selectedFacilities, Set<Equipment> selectedEquipment, boolean favourites, boolean ourFavourites) {
    List<Playground> filtered = new ArrayList<Playground>(playgrounds.size());
    for(Playground playground : playgrounds) {
      if(playground.facilities.keySet().containsAll(selectedFacilities) &&
        playground.equipment.keySet().containsAll(selectedEquipment) &&
        (playground.favourite || !favourites) &&
        (playground.ourFavourite || !ourFavourites)) {
        filtered.add(playground);
      }
    }

    // Log.v("ParkPlay", "filtered: " + filtered.size());

    return filtered;
  }

  public boolean favouritesExist() {
    for(Playground playground : playgrounds) {
      if(playground.favourite) return true;
    }
    return false;
  }

  public static Playground getNearestPlayground(List<Playground> playgrounds, Location location) {
    double closestDistance = Double.MAX_VALUE;
    Playground closestPlayground = null;

    for(Playground playground : playgrounds) {
      double distance = location.distanceTo(playground.getLocation());
      if(distance < closestDistance) {
        closestDistance = distance;
        closestPlayground = playground;
      }
    }

    return closestPlayground;
  }

  public List<Playground> getFilteredPlaygrounds(Set<Object> selectedItems) {
    Set<Facility> selectedFacilities = new HashSet<Facility>(facilities);
    selectedFacilities.retainAll(selectedItems);

    Set<Equipment> selectedEquipment = new HashSet<Equipment>(equipment);
    selectedEquipment.retainAll(selectedItems);

    boolean favouriteSelected = selectedItems.contains(FAVOURITES_ITEM);
    boolean ourFavouriteSelected = selectedItems.contains(OUR_FAVOURITES_ITEM);

    return getFilteredPlaygrounds(selectedFacilities, selectedEquipment, favouriteSelected, ourFavouriteSelected);
  }

  public List<Playground> getFilteredPlaygrounds(Settings settings) {
    return getFilteredPlaygrounds(settings.selectedFacilities, settings.selectedEquipment, settings.favouriteSelected, settings.ourFavouriteSelected);
  }

  public List<Playground> getFilteredPlaygrounds() {
    return getFilteredPlaygrounds(settings);
  }

  public static void populateDistance(List<Playground> playgrounds, Location origin) {
    for(Playground playground : playgrounds) {
      if(origin == null) {
        playground.distance = -1;
      } else {
        playground.distance = origin.distanceTo(playground.getLocation());
      }
    }
  }
}
