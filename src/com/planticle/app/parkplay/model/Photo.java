package com.planticle.app.parkplay.model;

import java.util.*;
import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;

public class Photo {
  public String name;
  public int width;
  public int height;
  public Playground playground;

  public String getUrl() {
    return getUrl("large");
  }

  public String getThumbnailUrl() {
    return getUrl("thumb");
  }

  private String getUrl(String type) {
    try {
      return String.format("https://s3-ap-southeast-2.amazonaws.com/goplay.planticle.com.au/%s/%s/%s", type,
        URLEncoder.encode(playground.name, "UTF-8"),
        URLEncoder.encode(name, "UTF-8"));
    } catch(UnsupportedEncodingException e) {
      // This should never happen
      throw new RuntimeException(e);
    }
  }
}
