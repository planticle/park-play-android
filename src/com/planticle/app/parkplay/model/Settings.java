package com.planticle.app.parkplay.model;

import java.util.*;

public class Settings {
  public enum SortMode {
    DISTANCE_FROM_ME,
    DISTANCE_FROM_PIN,
    ALPHABETICAL
  };

  public SortMode sortMode;
  public double sortPinLat;
  public double sortPinLon;

  public Set<Facility> selectedFacilities;
  public Set<Equipment> selectedEquipment;
  public boolean favouriteSelected;
  public boolean ourFavouriteSelected;

  public Settings() {
    reset();
  }

  public boolean isSortPinSet() {
    return sortPinLat != 0 || sortPinLon != 0;
  }

  public void reset() {
    sortMode = SortMode.DISTANCE_FROM_ME;
    // Adelaide CBD
    //sortPinLat = -34.9290;
    //sortPinLon = 138.6010;
    resetFilters();
  }

  public void resetFilters() {
    selectedFacilities = Collections.emptySet();
    selectedEquipment = Collections.emptySet();
    favouriteSelected = false;
    ourFavouriteSelected = false;
  }
}
