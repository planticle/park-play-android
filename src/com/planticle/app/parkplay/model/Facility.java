package com.planticle.app.parkplay.model;

import java.util.*;

public class Facility {
  public String description;
  public String name;

  public Facility(String name) {
    this.description = name;
    this.name = name;
  }

  public String getIconName() {
    return "ic_" + name.toLowerCase().replaceAll("[^a-z]", "_");
  }
}
