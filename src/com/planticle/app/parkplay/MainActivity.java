package com.planticle.app.parkplay;

import com.planticle.app.parkplay.model.*;
import com.planticle.app.parkplay.model.Settings.SortMode;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import android.app.Activity;
import android.os.Bundle;
import android.app.ProgressDialog;
import com.actionbarsherlock.app.*;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.view.*;
import android.graphics.*;
import android.view.WindowManager;
import android.util.Log;
import android.app.AlertDialog;
import android.widget.*;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.graphics.drawable.Drawable;
import android.content.DialogInterface;
import com.google.android.gms.location.*;
import com.google.android.gms.common.*;
import android.support.v4.app.NavUtils;
import android.content.Intent;

import java.lang.reflect.Field;
import java.util.*; 

public class MainActivity extends SherlockFragmentActivity
{
    LocationClient locationClient;
    TypefaceSpan.Factory textFactory = new TypefaceSpan.Factory(this, "RobotoSlab-Regular");
    Menu sortMenu;
    EventManager event = new EventManager();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        getWindow().setFormat(PixelFormat.RGBA_8888);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DITHER);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // Set up action bar
        ActionBar actionBar = getSupportActionBar();

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(" ");

        ActionBar.Tab tab = actionBar.newTab()
            .setText(textFactory.getString("Map"))
            .setTag("map")
            .setTabListener(new TabListener<MapFragment>(this, "map", MapFragment.class));
        actionBar.addTab(tab, 0, false);

        tab = actionBar.newTab()
            .setText(textFactory.getString("List"))
            .setTag("list")
            .setTabListener(new TabListener<ListFragment>(this, "list", ListFragment.class));
        actionBar.addTab(tab, 1, false);


        if(savedInstanceState == null) {

            // Set up initial state based on requested mode
            String mode = getIntent().getStringExtra("mode");
            if(mode == null) mode = "map";

            if(Data.getInstance() != null) Data.getInstance().settings.reset();

            if(mode.equals("map")) {
                getSupportActionBar().setSelectedNavigationItem(0);
            } else if (mode.equals("list")) {
                getSupportActionBar().setSelectedNavigationItem(1);
            } else if (mode.equals("my_favourites")) {
                if(Data.getInstance() != null) Data.getInstance().settings.favouriteSelected = true;
                getSupportActionBar().setSelectedNavigationItem(1);
            } else if (mode.equals("our_favourites")) {
                if(Data.getInstance() != null) Data.getInstance().settings.ourFavouriteSelected = true;
                getSupportActionBar().setSelectedNavigationItem(1);
            }
        }

        // Start location client and notify fragments when connected
        if(locationClient == null) {
            locationClient = new LocationClient(this, new GooglePlayServicesClient.ConnectionCallbacks() {
                public void onConnected(Bundle connectionHint) {
                    MapFragment mapFragment = getMapFragment();
                    if(mapFragment != null) mapFragment.locationClientConnected();
                    ListFragment listFragment = getListFragment();
                    if(listFragment != null) listFragment.locationClientConnected();
                }
                public void onDisconnected() { }
            }, new GooglePlayServicesClient.OnConnectionFailedListener() {
                public void onConnectionFailed(ConnectionResult result) { }
            });
            locationClient.connect();
        } else {
            Log.v("ParkPlay", "locationClient was non null on create");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        b.putInt("selectedTabIndex", getSupportActionBar().getSelectedNavigationIndex());
        super.onSaveInstanceState(b);
    }

    @Override
    public void onRestoreInstanceState(Bundle b) {
        super.onRestoreInstanceState(b);
        if (b.containsKey("selectedTabIndex")) {
            getSupportActionBar().setSelectedNavigationItem(b.getInt("selectedTabIndex"));
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Handle selecting a playground on the map (when coming from details 'Show on Map' button)
        try {
            final int id = getIntent().getIntExtra("playgroundId", -1);
            //Log.v("ParkPlay", "Playground id: " + id);

            if(id >= 0) {
                getIntent().removeExtra("playgroundId"); // remove the playground id so we don't do this the next time the activity is resumed
                Data.getInstanceAsync(this, new Data.Callback() {
                    public void run(Data data) {
                        showPlaygroundOnMap(id);
                    }
                });
            }
        } catch(Exception e) {
            Log.e("ParkPlay", "Exception while showing playground on map", e);
        }


        Data.getInstanceAsync(this, new Data.Callback() {
            public void run(Data data) {
                if(data.settings.favouriteSelected)
                    settingsUpdated();
            }
        });
    }

    private void showPlaygroundOnMap(final int id) {
        showMapFragment(); // show map tab
        whenFragmentReady("map", new Runnable() {
            public void run() {
                getMapFragment().selectPlayground(getData().getPlayground(id)); // center on playground
            }
        });
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        event.fire("fragment_attach_" + fragment.getTag());
    }

    private void whenFragmentReady(String tag, Runnable runnable) {
        Fragment f = getSupportFragmentManager().findFragmentByTag(tag);
        if(f == null) {
            event.once("fragment_attach_" + tag, runnable);
        } else {
            runnable.run();
        }
    }

    private MapFragment getMapFragment() {
        return (MapFragment) getSupportFragmentManager().findFragmentByTag("map");
    }

    private ListFragment getListFragment() {
        return (ListFragment) getSupportFragmentManager().findFragmentByTag("list");
    }

    private void showMapFragment() {
        getSupportActionBar().setSelectedNavigationItem(0);
    }
    
    private void showListFragment() {
        getSupportActionBar().setSelectedNavigationItem(1);
    }

    private static final int MENU_FILTER_ID = 1;
    private static final int MENU_SORT_ID = 2;
    private static final int MENU_ABOUT_ID = 3;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        menu.add(0, MENU_FILTER_ID, 0, textFactory.getString("Filter")).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        
        //menu.add(0, MENU_SORT_ID, 1, textFactory.getString("Sort"));
        
        SubMenu subMenu = menu.addSubMenu(0, MENU_SORT_ID, 1, textFactory.getString("Sort"));
        subMenu.add(1, SortMode.DISTANCE_FROM_ME.ordinal(), 0, "Distance from me").setChecked(true);
        subMenu.add(1, SortMode.DISTANCE_FROM_PIN.ordinal(), 1, "Distance from pin");
        subMenu.add(1, SortMode.ALPHABETICAL.ordinal(), 2, "Alphabetically");
        subMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        subMenu.setGroupCheckable(1, true, true); // radio button behaviour

        sortMenu = subMenu; // save for later
        
        MenuItem item = menu.add(0, MENU_ABOUT_ID, 2, "About");
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        item.setIcon(R.drawable.about);

        return true;
    }

    private MenuItem getSortMenuItem(SortMode mode) {
        if(sortMenu == null || mode == null) return null;
        return sortMenu.findItem(mode.ordinal());
    }

/*
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        String tab = getSupportActionBar().getSelectedTab().getTag();
        menu.findItem(MENU_SORT_ID).setVisible(tab.equals("list"))
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if(item.getGroupId() == 0) {
            switch (item.getItemId()) {
            case MENU_FILTER_ID:
                Data.getInstanceAsync(this, new Data.Callback() {
                    public void run(Data data) {
                        showFilterDialog();
                    }
                });
                return true;
            case MENU_ABOUT_ID:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            }
        } else if(item.getGroupId() == 1) {
            SortMode mode = SortMode.values()[item.getItemId()];

            if(mode == SortMode.DISTANCE_FROM_PIN && !getData().settings.isSortPinSet()) {
                showMapFragment();
                AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("Drop Pin")
                    .setMessage("Tap and hold on the map to search for nearby playgrounds.")
                    .setPositiveButton("OK", null)
                    .show();
            } else {
                item.setChecked(true);
                getData().settings.sortMode = mode;
                showListFragment();
                ListFragment listFragment = getListFragment();
                if(listFragment != null) {
                    listFragment.sortPlaygrounds();
                }
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showFilterDialog() {
        final Data data = getData();

        List<Object> items = new ArrayList<Object>();
        items.add(Data.FAVOURITES_ITEM);
        items.add(Data.OUR_FAVOURITES_ITEM);
        items.addAll(data.facilities);
        items.addAll(data.equipment);

        final Set<Object> selectedItems = data.getSelectedItems();
        final Set<Object> unselectableItems = new HashSet<Object>();

        unselectableItems.addAll(data.getUnselectableItems(selectedItems));

        final ArrayAdapter listAdapter = new ArrayAdapter(this, R.layout.checkbox_list_item, R.id.name, items) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                Object item = getItem(position);
                View view = super.getView(position, convertView, parent);
                TextView tv = (TextView)view.findViewById(R.id.name);
                CheckBox cb = (CheckBox)view.findViewById(R.id.checkbox);

                String name = "";
                String iconName = "";
                if(item == Data.FAVOURITES_ITEM) {
                    name = "My Favourites";
                    iconName = "ic_favourite";
                } else if(item == Data.OUR_FAVOURITES_ITEM) {
                    name = "Park Play's Favourites";
                    iconName = "ic_favourite";
                } else if(item instanceof Facility) {
                    name = ((Facility)item).name;
                    iconName = "ic_" + name.toLowerCase().replaceAll("[^a-z]", "_");
                } else if(item instanceof Equipment) {
                    name = ((Equipment)item).name;
                    iconName = "ic_" + name.toLowerCase().replaceAll("[^a-z]", "_");
                }
                
                tv.setText(name);
                tv.setCompoundDrawablesWithIntrinsicBounds(getId(iconName, R.drawable.class), 0, 0, 0);

                boolean enabled = !unselectableItems.contains(item);
                view.setEnabled(enabled);
                view.setClickable(!enabled);
                //view.setFocusable(enabled);

                com.nineoldandroids.view.ViewHelper.setAlpha(view, enabled ? 1.0f : 0.3f);
                //view.setAlpha(enabled ? 1.0f : 0.3f);

                cb.setChecked(selectedItems.contains(item));
                
                return view;
            }
        };

        ListView listView = new ListView(this);
        listView.setCacheColorHint(0);
        listView.setAdapter(listAdapter);

        final AlertDialog dialog = new AlertDialog.Builder(this)
            .setTitle("Filter Playgrounds (" + data.getFilteredPlaygrounds(data.settings).size() + ")")
            .setInverseBackgroundForced(true)
            .setNegativeButton("Cancel", null)
            .setNeutralButton("Reset", null)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Settings settings = data.settings;
                    settings.selectedFacilities = new HashSet<Facility>(data.facilities);
                    settings.selectedFacilities.retainAll(selectedItems);
                    settings.selectedEquipment = new HashSet<Equipment>(data.equipment);
                    settings.selectedEquipment.retainAll(selectedItems);
                    settings.favouriteSelected = selectedItems.contains(Data.FAVOURITES_ITEM);
                    settings.ourFavouriteSelected = selectedItems.contains(Data.OUR_FAVOURITES_ITEM);
                    settingsUpdated();
                }
            })
            .setView(listView)
            .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        data.settings.resetFilters();
                        selectedItems.clear();
                        unselectableItems.clear();
                        listAdapter.notifyDataSetChanged();
                        dialog.setTitle("Filter Playgrounds (" + data.getFilteredPlaygrounds(selectedItems).size() + ")");
                    }
                });
            }
        });

        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> listView, View itemView, int position, long itemId) {
                //Log.v("ParkPlay", "Item pressed " + position);
                Object item = listAdapter.getItem(position);
                if(selectedItems.contains(item)) {
                    selectedItems.remove(item);
                } else {
                    selectedItems.add(item);
                }
                unselectableItems.clear();
                unselectableItems.addAll(data.getUnselectableItems(selectedItems));
                listAdapter.notifyDataSetChanged();
                dialog.setTitle("Filter Playgrounds (" + data.getFilteredPlaygrounds(selectedItems).size() + ")");
            }
        });

        dialog.show();
    }

    public void sortModeUpdated(boolean showList) {
        ListFragment listFragment = getListFragment();
        if(listFragment != null) listFragment.sortPlaygrounds();
        if(showList) showListFragment();

        // Check the associated menu item
        MenuItem menuItem = getSortMenuItem(getData().settings.sortMode);
        if(menuItem != null) menuItem.setChecked(true);
    }

    private void settingsUpdated() {
        List<Playground> filteredPlaygrounds = getData().getFilteredPlaygrounds();

        // Handle situation where no playgrounds match the criteria
        if(filteredPlaygrounds.size() == 0) {
            if(getData().settings.favouriteSelected && !getData().favouritesExist()) {
                // Show alert: you don't have any favourites yet
                new AlertDialog.Builder(this)
                    .setTitle("Favourites")
                    .setMessage("You do not have any favourites. Add favourites by tapping the star icon on any playground details screen.")
                    .setInverseBackgroundForced(true)
                    .setPositiveButton("OK", null)
                    .show();
            }
            getData().settings.favouriteSelected = false;
            filteredPlaygrounds = getData().getFilteredPlaygrounds();
        }

        if(locationClient.isConnected()) {
            getData().populateDistance(filteredPlaygrounds, locationClient.getLastLocation());
        }

        MapFragment mapFragment = getMapFragment();
        if(mapFragment != null) mapFragment.updatePlaygrounds(filteredPlaygrounds);
        ListFragment listFragment = getListFragment();
        if(listFragment != null) listFragment.updatePlaygrounds(filteredPlaygrounds);
    }

    public static int getId(String resourceName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resourceName);
            return idField.getInt(idField);
        } catch (Exception e) {
            //throw new RuntimeException("No resource ID found for: " + variableName + " / " + c, e);
            return 0;
        }
    }

    public Data getData() {
        return Data.getInstance();
    }

    public static class TabListener<T extends Fragment> implements ActionBar.TabListener{
        private Fragment mFragment;
        private final Activity mActivity;
        private final String mTag;
        private final Class<T> mClass;

        public TabListener(SherlockFragmentActivity activity, String tag, Class<T> clz) {
            mActivity = activity;
            mTag = tag;
            mClass = clz;

            // Needed this to stop multiple fragments getting instantiated accross activity restarts
            mFragment = activity.getSupportFragmentManager().findFragmentByTag(tag);
        }

        public void onTabSelected(Tab tab, FragmentTransaction ft) {
            // Check if the fragment is already initialized
            if (mFragment == null) {
                // If not, instantiate and add it to the activity
                mFragment = (Fragment) Fragment.instantiate(mActivity, mClass.getName());

                //mFragment.setProviderId(mTag); // id for event provider

                ft.add(android.R.id.content, mFragment, mTag);
            } else {
                // If it exists, simply attach it in order to show it
                ft.attach(mFragment);
            }
        }

        public void onTabUnselected(Tab tab, FragmentTransaction ft) {
            if (mFragment != null) {
                // Detach the fragment, because another one is being attached
                ft.detach(mFragment);
            }
        }

        public void onTabReselected(Tab tab, FragmentTransaction ft) {
            //TODO
        }
    }
}
