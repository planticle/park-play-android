package com.planticle.app.parkplay;

import com.planticle.app.parkplay.model.*;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.PixelFormat;
import android.view.View;
import android.graphics.*;
import android.graphics.drawable.*;
import android.widget.*;
import android.content.*;
import android.util.Log;
import android.support.v4.app.NavUtils;
import android.text.*;
import android.text.method.*;
import android.net.Uri;

import com.squareup.picasso.Picasso;
import com.actionbarsherlock.app.*;
import com.actionbarsherlock.view.*;

public class PlaygroundDetailsActivity extends SherlockActivity
{
    private Playground playground;
    private MenuItem favouriteToggle;
    private FavouritesManager favouritesManager;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.playground_details);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Bundle b = getIntent().getExtras();

        favouritesManager = new FavouritesManager(this);

        final int id = b.getInt("playgroundId");
        Data.getInstanceAsync(this, new Data.Callback() {
            public void run(Data data) {
                populate(data.getPlayground(id));
            }
        });

        for(int viewId : new int[] {R.id.directions, R.id.show_on_map, R.id.view_photos}) {
            AndroidUtil.setTypefaceForAllDescendantTextViews(this, findViewById(viewId), "RobotoSlab-Regular");    
        }
        //Picasso.with(this).setDebugging(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuItem item = menu.add(0, 1, 0, "Favourite");
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        favouriteToggle = item;
        updateFavouriteToggle();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case 1:
            if(playground != null) {
                playground.favourite = !playground.favourite;
                if(playground.favourite) {
                    favouritesManager.addFavourite(playground.id);
                    Toast.makeText(this, "Playground added to favourites", 0).show();
                } else {
                    favouritesManager.removeFavourite(playground.id);
                    Toast.makeText(this, "Playground removed from favourites", 0).show();
                }
                updateFavouriteToggle();
            }
            return true;
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void updateFavouriteToggle() {
        if(playground != null) {
            favouriteToggle.setIcon(playground.favourite ? R.drawable.favourite_on : R.drawable.favourite_off);
        }
    }

    private void populate(Playground playground) {
        getSupportActionBar().setTitle(playground.name);

        updateFavouriteToggle();

        TextView address = (TextView) findViewById(R.id.address);
        address.setText(playground.street + "\n" + playground.suburb);

        Button viewPhotos = (Button) findViewById(R.id.view_photos);
        viewPhotos.setText("View Photos (" + playground.photos.size() + ")");

        LinearLayout facilities = (LinearLayout) findViewById(R.id.facilities);
        for(Facility facility : playground.facilities.keySet()) {
            facilities.addView(getFeatureView(facility.name, facility.getIconName(), playground.facilities.get(facility)));
        }

        LinearLayout equipment = (LinearLayout) findViewById(R.id.equipment);
        for(Equipment equip : playground.equipment.keySet()) {
            equipment.addView(getFeatureView(equip.name, equip.getIconName(), playground.equipment.get(equip)));
        }

        if(playground.photos.size() > 0) {
            ImageView thumbnail = (ImageView) findViewById(R.id.thumbnail);
            Log.v("ParkPlay", "Thumb url: " + playground.photos.get(0).getThumbnailUrl());
            Picasso.with(this)
                .load(playground.photos.get(0).getThumbnailUrl())
                .skipMemoryCache()
                .placeholder(new ColorDrawable(0x44000000))
                .fit()
                .into(thumbnail);
        }

        this.playground = playground;
    }

    private View getFeatureView(String name, String iconName, int count) {
        View view = getLayoutInflater().inflate(R.layout.playground_feature, null);

        TextView tv = (TextView) view.findViewById(R.id.name);
        tv.setText(name);
        int iconId = MainActivity.getId(iconName, R.drawable.class);
        tv.setCompoundDrawablesWithIntrinsicBounds(iconId, 0, 0, 0);
        tv.setTextColor(0xff333333);

        tv = (TextView) view.findViewById(R.id.count);
        tv.setText(count == -1 ? "✓" : (" " + count));
        tv.setTextColor(0xff333333);

        return view;
    }

    public void onPhotosButtonClick(View v) {
        Intent intent = new Intent(this, PlaygroundPhotosActivity.class);
        intent.putExtra("playgroundId", playground.id);
        startActivity(intent);
    }

    public void onDirectionsButtonClick(View v) {
        launchDirections();
    }

    public void onMapButtonClick(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("playgroundId", playground.id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void launchDirections() {
        if(playground == null) return;
        Uri uri = Uri.parse("http://maps.google.com/maps?saddr=&daddr=" + playground.lat + "," + playground.lon);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }
}
