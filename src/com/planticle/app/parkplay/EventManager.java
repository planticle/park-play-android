package com.planticle.app.parkplay;

import java.util.*;
import android.util.Log;

public class EventManager {
    private Map<String, List<Listener>> listeners = new HashMap<String, List<Listener>>();


    private static class Listener {
        public Listener(Runnable r, boolean once) {
            this.r = r;
            this.once = once;
        }

        Runnable r;
        boolean once;
    }

    public void on(String event, Runnable r) {
        put(event, new Listener(r, false));
    }

    public void once(String event, Runnable r) {
        put(event, new Listener(r, true));
    }

    public void off(String event, Runnable r) {
        List<Listener> eventListeners = listeners.get(event);
        if(eventListeners != null) {
            Iterator<Listener> iter = eventListeners.iterator();
            while(iter.hasNext()) {
                if(iter.next().r == r) iter.remove();
            }
            if(eventListeners.isEmpty()) listeners.remove(event);
        }
    }

    public void off(String event) {
        listeners.remove(event);
    }

    public void off(Runnable r) {
        for(String event : listeners.keySet()) {
            off(event, r);
        }
    }

    private void on(String event, Runnable r, boolean once) {
        Listener l = new Listener(r, once);
    }

    private void put(String event, Listener l) {
        List<Listener> eventListeners = listeners.get(event);
        if(eventListeners == null) {
            eventListeners = new ArrayList<Listener>();
            listeners.put(event, eventListeners);
        }
        eventListeners.add(l);
    }

    public void fire(String event) {
        List<Listener> eventListeners = listeners.get(event);
        if(eventListeners != null) {
            Iterator<Listener> iter = eventListeners.iterator();
            while(iter.hasNext()) {
                Listener l = iter.next();
                try {
                    l.r.run();
                } catch(Exception e) {
                    Log.e("ParkPlay", "Exception in listener", e);
                }
                if(l.once) iter.remove();
            }
            if(eventListeners.isEmpty()) listeners.remove(event);
        }
    }

    /** Begin micro unit test **/

    private static Runnable append(final StringBuffer sb, final String s) {
        return new Runnable() {
            public void run() {
                sb.append(s);
            }
        };
    }

    public static void test() {
        EventManager em = new EventManager();
        StringBuffer result = new StringBuffer();

        Runnable a1 = append(result, "a1");
        Runnable a2 = append(result, "a2");
        Runnable a3 = append(result, "a3");
        Runnable b1 = append(result, "b1");
        Runnable b2 = append(result, "b2");

        em.on("a", a1);
        em.on("a", a2);
        em.once("a", a3);
        em.on("b", b1);
        em.on("b", b2);

        em.fire("a"); //a1a2a3
        em.fire("a"); //a1a2
        em.fire("b"); //b1b2

        em.off("a", a1);

        em.fire("a"); //a2
        em.fire("b"); //b1b2

        em.off(b1);

        em.fire("b"); //b2

        em.off("a");

        em.fire("a"); //

        Log.v("ParkPlay", result.toString());
        Log.v("ParkPlay", "a1a2a3a1a2b1b2a2b1b2b2");
        Log.v("ParkPlay", "OK: " + result.toString().equals("a1a2a3a1a2b1b2a2b1b2b2"));
    }
}
