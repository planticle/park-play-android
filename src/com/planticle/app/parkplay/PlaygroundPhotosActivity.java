package com.planticle.app.parkplay;

import com.planticle.app.parkplay.model.*;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.PixelFormat;
import android.view.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.widget.*;
import android.content.*;
import android.util.Log;
import android.support.v4.app.NavUtils;
import android.text.*;
import android.text.method.*;
import android.net.Uri;
import android.animation.*;

import com.squareup.picasso.*;
import com.actionbarsherlock.app.*;
import com.actionbarsherlock.view.MenuItem;

public class PlaygroundPhotosActivity extends SherlockActivity
{
    private Playground playground;
    private BaseAdapter adapter;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.playground_photos);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        GridView gridView = (GridView) findViewById(R.id.photos);

        // TODO - clean up this switcheroo.. just experimenting for now
        ImageView imageView = (ImageView) findViewById(R.id.expanded_image);
        FrameLayout container = (FrameLayout) findViewById(R.id.container);
        container.removeView(imageView);
        imageView = new TouchImageView(this);
        imageView.setId(R.id.expanded_image);
        imageView.setVisibility(View.INVISIBLE);
        container.addView(imageView);

        final Bitmap ribbon = BitmapFactory.decodeResource(getResources(), R.drawable.awesome_ribbon);

        adapter = new BaseAdapter() {
            public int getCount() {
                return playground == null ? 0 : playground.photos.size();
            }

            public Object getItem(int position) {
                return playground.photos.get(position);
            }

            public long getItemId(int position) {
                return 0;
            }

            // create a new ImageView for each item referenced by the Adapter
            public View getView(int position, View convertView, ViewGroup parent) {
                ImageView imageView;
                if (convertView == null) {  // if it's not recycled, initialize some attributes
                    imageView = new ImageView(PlaygroundPhotosActivity.this) {
                        @Override
                        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
                            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                            setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
                        }
                    };
                    //imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imageView.setBackgroundColor(0x22000000);
                    //imageView.setPadding(8, 8, 8, 8);
                    //imageView.setImageResource(R.drawable.placeholder);
                } else {
                    imageView = (ImageView) convertView;
                }


                Photo photo = (Photo) getItem(position);

                Picasso.with(PlaygroundPhotosActivity.this)
                    .load(photo.getThumbnailUrl())
                    .skipMemoryCache()
                    .placeholder(R.drawable.placeholder)
                    .fit()
                    .transform(new RibbonTransformation(photo.name, ribbon))
                    .into(imageView);
                return imageView;
            }
        };

        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v("ParkPlay", "position: " + position);
                Photo photo = (Photo)adapter.getItem(position);

                Intent intent = new Intent(PlaygroundPhotosActivity.this, ViewPhotoActivity.class);
                intent.putExtra("playgroundId", playground.id);
                intent.putExtra("photoIndex", position);
                startActivity(intent);

                //zoomImageFromThumb(view, photo.getUrl());
            }
        });


        Bundle b = getIntent().getExtras();

        final int id = b.getInt("playgroundId");
        Data.getInstanceAsync(this, new Data.Callback() {
            public void run(Data data) {
                populate(data.getPlayground(id));
            }
        });
    }

    private void populate(Playground playground) {
        getSupportActionBar().setTitle(playground.name);
        this.playground = playground;
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}

class RibbonTransformation implements Transformation {
    private static Bitmap ribbon;
    private boolean process;

    public RibbonTransformation(String fileName, Bitmap ribbon) {
        this.ribbon = ribbon;
        process = fileName.toLowerCase().indexOf("awesome") >= 0;
    }

    @Override public Bitmap transform(Bitmap source) {
        if(process) {
            Bitmap.Config config = source.getConfig();
            // set default bitmap config if none
            if(config == null) {
                config = Bitmap.Config.ARGB_8888;
            }
            Bitmap result = source.copy(config, true);

            Canvas canvas = new Canvas(result);
            //canvas.scale(0.5f, 0.5f);
            canvas.drawBitmap(ribbon, 0, 0, new Paint());

            source.recycle();
            return result;
        } else {
            return source;
        }
    }

    @Override public String key() { return "ribbon(" + process + ")"; }
}
