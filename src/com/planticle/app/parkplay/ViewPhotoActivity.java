package com.planticle.app.parkplay;

import com.planticle.app.parkplay.model.*;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.PixelFormat;
import android.view.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.widget.*;
import android.content.*;
import android.util.Log;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.*;
import android.text.method.*;
import android.net.Uri;
import android.animation.*;
import android.view.animation.*;

import com.squareup.picasso.Picasso;
import com.actionbarsherlock.app.*;

public class ViewPhotoActivity extends SherlockActivity
{
    private Playground playground;
    private ViewPager viewPager;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.playground_photos_gallery);

        viewPager = (ViewPager) findViewById(R.id.photo_pager);
        setupViewPager();

        loadPhotos();
    }

    private void setupViewPager() {
        viewPager.setPageMargin(10);
    }

    private void loadPhotos() {
        Bundle b = getIntent().getExtras();

        final int playgroundID = b.getInt("playgroundId");
        final int initialPhoto = b.getInt("photoIndex");
        Data.getInstanceAsync(this, new Data.Callback() {
            public void run(Data data) {
                playground = data.getPlayground(playgroundID);
                viewPager.setAdapter(new ImagePagerAdaptor());
                viewPager.setCurrentItem(initialPhoto);
            }
        });
    }

    private class ImagePagerAdaptor extends PagerAdapter {
        private LayoutInflater inflater;

        public ImagePagerAdaptor() {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return playground == null ? 0 : playground.photos.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View viewLayout = inflater.inflate(
                    R.layout.playground_photos_gallery_photo,
                    container, false);
            final ImageView imageView = (ImageView) viewLayout.findViewById(R.id.photo_gallery_photo);

            imageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    finish();
                }
            });


            final int index = position;
            Data.getInstanceAsync(ViewPhotoActivity.this, new Data.Callback() {
                public void run(Data data) {
                    Picasso.with(ViewPhotoActivity.this)
                        .load(playground.photos.get(index).getUrl())
                        .skipMemoryCache()
                        .placeholder(R.drawable.placeholder_large)
                    .into(imageView);
                }
            });


            container.addView(viewLayout);
            return viewLayout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
