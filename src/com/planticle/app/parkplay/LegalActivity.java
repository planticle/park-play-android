package com.planticle.app.parkplay;

import com.planticle.app.parkplay.model.*;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.PixelFormat;
import android.view.View;
import android.graphics.*;
import android.graphics.drawable.*;
import android.widget.*;
import android.content.*;
import android.util.Log;
import android.support.v4.app.NavUtils;
import android.text.*;
import android.text.method.*;
import android.net.Uri;

import org.apache.commons.io.IOUtils;
import com.squareup.picasso.Picasso;
import com.actionbarsherlock.app.*;
import com.actionbarsherlock.view.*;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class LegalActivity extends SherlockActivity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.legal);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView textView = (TextView) findViewById(R.id.text);    
        textView.setText(GooglePlayServicesUtil.getOpenSourceSoftwareLicenseInfo(this));
        AndroidUtil.setTypefaceForAllDescendantTextViews(this, textView, "RobotoSlab-Regular");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}
