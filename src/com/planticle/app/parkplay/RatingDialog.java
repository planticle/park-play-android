package com.planticle.app.parkplay;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.text.format.DateUtils;
import android.view.*;
import android.util.Log;
import android.widget.RatingBar;

import com.google.analytics.tracking.android.*;

public class RatingDialog {
    private static final boolean SHOW_DIALOG_OVERRIDE = false;

    private static final int MINIMUM_LAUNCHES_BEFORE_SHOW = 3;
    private static final int MINIMUM_DAYS_BEFORE_SHOW = 2;
    private static final int MINIMUM_DAYS_BEFORE_REMIND = 7;

    private final Context context;

    private long firstLaunchTime;
    private long launchCount;
    private boolean doNotShow;

    public RatingDialog(Context context) {
        this.context = context;


        SharedPreferences prefs = context.getSharedPreferences("rating-dialog", 0);

        launchCount = prefs.getLong("launch-count", 0) + 1;
        firstLaunchTime = prefs.getLong("first-launch", 0);
        doNotShow = prefs.getBoolean("never-show-again", false);

        updateState(prefs);
    }

    private void updateState(SharedPreferences prefs) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("launch-count", launchCount);
        editor.putBoolean("never-show-again", doNotShow);

        if (firstLaunchTime == 0) { // if not set yet
            firstLaunchTime = System.currentTimeMillis();
            editor.putLong("first-launch", firstLaunchTime);
        }


        if (Build.VERSION.SDK_INT < 9)
            editor.commit();
        else
            editor.apply();
    }


    public void showRatingDialogIfNeeded() {
        if (needsToShowDialog())
            showRatingDialog();
    }


    private boolean needsToShowDialog() {
        if (SHOW_DIALOG_OVERRIDE)
            return true;

        if (doNotShow)
            return false;

        if (launchCount < MINIMUM_LAUNCHES_BEFORE_SHOW)
            return false;

        long now = System.currentTimeMillis();
        if (now < (firstLaunchTime + MINIMUM_DAYS_BEFORE_SHOW * DateUtils.DAY_IN_MILLIS))
            return false;

        return true;
    }


    private void showRatingDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.rating_dialog_title);

        View ratingDialogContent = LayoutInflater.from(context)
                                                 .inflate(R.layout.rating_dialog_rating, null);
        dialog.setView(ratingDialogContent);

        final RatingBar ratingBar = (RatingBar) ratingDialogContent.findViewById(R.id.ratingbar);

        dialog.setNeutralButton(R.string.rating_later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                remindMeLater();
                dialog.dismiss();
            }
        });

        dialog.setNegativeButton(R.string.rating_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                neverAskAgain();
                dialog.dismiss();
            }
        });
        dialog.setPositiveButton(R.string.rating_submit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                submitRating(ratingBar);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void remindMeLater() {
        firstLaunchTime = firstLaunchTime - MINIMUM_DAYS_BEFORE_SHOW + MINIMUM_DAYS_BEFORE_REMIND;
        updateState(context.getSharedPreferences("rating-dialog", 0));
    }

    private void neverAskAgain() {
        doNotShow = true;
        updateState(context.getSharedPreferences("rating-dialog", 0));
    }

    private void submitRating(RatingBar ratingBar) {
        int rating = (int) Math.round(ratingBar.getRating());

        saveRating(rating);

        doNotShow = true;
        updateState(context.getSharedPreferences("rating-dialog", 0));

        if(rating < 4)
            askToComment();
        else
            askToReview();
    }

    private void saveRating(int rating) {
        EasyTracker tracker = EasyTracker.getInstance(context);
        tracker.send(MapBuilder
            .createEvent("rating",
                         "submit",
                         "rating_value_" + rating,
                         (long)rating)
            .set(Fields.customDimension(1), String.valueOf(rating))
            .build()
        );
    }

    private void askToComment() {
        showYesNoDialog(R.string.rating_negative_message,
                        new Runnable() {
                            @Override
                            public void run() {
                                constructCommentEmail();
                            }
                        });
    }

    private void constructCommentEmail() {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"contact@planticle.com.au"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Possible Improvements to Park Play");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Your comments on Park Play...");

        context.startActivity(Intent.createChooser(emailIntent, "Send comments..."));
    }

    private void askToReview() {
        showYesNoDialog(R.string.rating_positive_message,
                        new Runnable() {
                            @Override
                            public void run() {
                                goToGooglePlay();
                            }
                        });
    }

    private void goToGooglePlay() {
        context.startActivity(new Intent(Intent.ACTION_VIEW,
            Uri.parse("market://details?id=" + context.getPackageName())));
    }

    private void showYesNoDialog(int messageId, final Runnable yesAction) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.rating_dialog_title);
        dialog.setMessage(messageId);

        dialog.setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                yesAction.run();
            }
        });

        dialog.show();
    }
}
